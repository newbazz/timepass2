#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction  
  This document builds the functionality with JavaScript to
  add behaviour/interactivity to the "Simple Bubble Sort"
  demonstration artefact(HTML).

* Bubble sort class

+ Creates Selection_sort class, which has all the variables required during its running
+ We use "let" to create an object of this class
+ iteratro1 -> Used to keep track while traversing the array
+ iterator2 -> Used to keep track while traversing the array(iterator1 + 1)
+ finished -> Determines whether or not the sort is finished
+ action -> Determines the current action being performed
+ fn_name -> Name of the function (Bubble sort here)
+ card -> Variable that corresponds to DOM elements that represent the array
+ comparisons -> Counts the number of comparisons
+ swaps -> Counts the number of swaps
+ operation -> Determined current operation
+ interval -> Varialble corresponding to setting and unsetting the slider time interval
+ num -> Variable that stores the array
+  Array has been initialized to contain all zero values to allocate the memory at the start itse

#+NAME: practice-universal-class
#+BEGIN_SRC js

class Bubble_sort{
  constructor(){
    this.iterator1 = 0;
    this.iterator2 = 0;
    this.finished = false;
    this.numOfCards = 8;
    this.action = 0;
    this.fn_name = "";
    this.card;
    this.comparisons = 0;
    this.swaps = 0;
    this.operation = "";
    this.interval = 0;
    this.num = [];
  };
};
let bubble_artefact = new Bubble_sort();
#+END_SRC

* Main Function
A function that performs the body onload functions
#+NAME: main-functions
#+BEGIN_SRC js
function main_functions()
{ 
  randomise();
  start_sort();
  handlers();
};
document.body.onload = function() {main_functions();}
#+END_SRC

* Adds all event handlers
A function that adds all the event handlers to the html
document.
#+NAME: handlers
#+BEGIN_SRC js
    function handlers(){
    document.getElementById("next").onclick = function() {check_next();};
    document.getElementById("swap").onclick = function() {check_swap();};
    document.getElementById("reset").onclick = function() {reload();};
    };

#+END_SRC


* Function Randomise

+ Creates a randomised array of defined size
+ Places these random values inside DOM cards
#+NAME: practice-randomise
#+BEGIN_SRC js

function randomise()
{ 
  var classToFill = document.getElementById("cards");
    for (var i = 0; i < bubble_artefact.numOfCards; i++){
        bubble_artefact.num[i] = Math.floor(Math.random() * 90 + 10);
        var temp = document.createElement("div");
        temp.className = "card";
        temp.innerHTML = bubble_artefact.num[i];
        temp.style.fontStyle = "normal";
        temp.style.color = "white";
        classToFill.appendChild(temp);
    }
};


#+END_SRC

* Function compare

+ Compares two given numbers given as input to it
+ Highlights numbers that are under consideration by changing their background colour
+ Returns True if the swap has to be performed
+ Return False otherwise

#+NAME: practice-compare
#+BEGIN_SRC js

function compare(i, j)
{
  bubble_artefact.comparisons++;
  for(var n = 0; n < bubble_artefact.numOfCards; n++)
  {
    if(n == i || n == j) { bubble_artefact.card[n].style.backgroundColor = "#a4c652"; } else { bubble_artefact.card[n].style.backgroundColor = "#288ec8"; }
  }
  if(eval(bubble_artefact.card[j].innerHTML) < eval(bubble_artefact.card[i].innerHTML))
    return true;
  else
    return false;
};


#+END_SRC

* Function Swap

+ Swaps the two elements whose indices are given as input

#+NAME: practice-swap
#+BEGIN_SRC js

function swap(i, j)
{
  bubble_artefact.swaps++;
  var temp;
  document.getElementById("ins").innerHTML += "<p>Swapping " + bubble_artefact.card[i].innerHTML + " and " + bubble_artefact.card[j].innerHTML + "</p>";
  temp = eval(bubble_artefact.card[j].innerHTML);
  bubble_artefact.card[j].innerHTML = eval(bubble_artefact.card[i].innerHTML);
  bubble_artefact.card[i].innerHTML = temp;
};

var n = 0;

#+END_SRC

* Function Bubble

+ Main driving function for this sorting algorithm
+ Updates the values of iterator1 and iterator2 and checks for termination conditions

#+NAME: practice-bubblesort
#+BEGIN_SRC js

function bubble()
{
  if(bubble_artefact.iterator1 < bubble_artefact.numOfCards-2)
  {
    bubble_artefact.iterator1++;
    bubble_artefact.iterator2++;
  }
  else
  {
    if(bubble_artefact.finished)
    {
      document.getElementById("ins").innerHTML = "<h3>The sort is complete</h3>";
      document.getElementById("next").style.backgroundColor = "grey";
      document.getElementById("next").disabled = true;
      document.getElementById("swap").style.backgroundColor = "grey";
      document.getElementById("swap").disabled = true;
    }
    else
    {
      bubble_artefact.finished = true;
      n++;
      bubble_artefact.iterator1 = 0;
      bubble_artefact.iterator2 = 1;
    }
  }
};


#+END_SRC

* Function Check Swap

+ Checks if the operation performed is correct or not
+ Send appropriate messages

#+NAME: practice-check-swap
#+BEGIN_SRC js

function check_swap(){
  var temp = compare(bubble_artefact.iterator1, bubble_artefact.iterator2);
  if(temp == 1)
  {
    swap(bubble_artefact.iterator1, bubble_artefact.iterator2);
    document.getElementById("ins").innerHTML = "Correct!";
    window[bubble_artefact.fn_name]();
    compare(bubble_artefact.iterator1, bubble_artefact.iterator2);
  }else
    document.getElementById("ins").innerHTML = "Incorrect, Think again!";    
  if(n!=bubble_artefact.numOfCards-1)
    bubble_artefact.finished = false;
  else
    bubble_artefact.finished = true;
};

#+END_SRC

* Function Check Next

+ Checks if the operation performed is correct or not
+ Send appropriate messages
#+NAME: practice-check-next
#+BEGIN_SRC js

function check_next(){
  var temp = compare(bubble_artefact.iterator1, bubble_artefact.iterator2);
  if(temp == 1)
    document.getElementById("ins").innerHTML = "Incorrect, Think again! Do we need to swap here?"; 
  else
  {
    document.getElementById("ins").innerHTML = "Correct!";
    window[bubble_artefact.fn_name]();
    compare(bubble_artefact.iterator1, bubble_artefact.iterator2);
  }
  if(n!=bubble_artefact.numOfCards-1)
    bubble_artefact.finished = false;
  else
    bubble_artefact.finished = true;
};

#+END_SRC

* Function Start sort

+ Called when Start is clicked on the webpage
+ Starts the sorting regime
+ Initializes variables that are going to be used
+ Calls the required function based on the sorting algorithm
#+NAME: practice-start-sort
#+BEGIN_SRC js

function start_sort()
{
  document.getElementById("comment-box-smaller").style.visibility = "visible";
  bubble_artefact.card = document.querySelectorAll('.card');
  bubble_artefact.action = 1;
  bubble_artefact.finished = true;
  bubble_artefact.comparisons = 0;
  bubble_artefact.swaps = 0;
  bubble_artefact.fn_name = "bubble";

  bubble_artefact.iterator1 = 0;
  bubble_artefact.iterator2 = 1;
  bubble_artefact.operation = "Swap";
  compare(bubble_artefact.iterator1, bubble_artefact.iterator2);

  document.getElementById("swap").style.visibility = "visible";
  document.getElementById("next").style.visibility = "visible";

  document.getElementById("swap").onclick = function() { check_swap(); };
  document.getElementById("next").onclick = function() { check_next(); };

  document.getElementById("next").disabled = false;
};

 #+END_SRC
* Function reload

+ Used to reload the artefact when Reset button is clicked
#+NAME: practice-reload
#+BEGIN_SRC js
function reload(){
  location.reload(true);
};

#+END_SRC

* Tangle
#+BEGIN_SRC js :tangle bubble_practice.js :eval no :noweb yes
<<practice-universal-class>>
<<main-functions>>
<<handlers>>
<<practice-randomise>>
<<practice-compare>>
<<practice-swap>>
<<practice-bubblesort>>
<<practice-check-swap>>
<<practice-check-next>>
<<practice-start-sort>>
<<practice-reload>>
#+END_SRC
