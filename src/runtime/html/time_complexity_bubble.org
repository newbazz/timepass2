#+TITLE: Demonstration artefact for Time complexity of Optimised Bubble Sort
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  Artefact counts number of comparisons for sorted, reverse
  sorted and randomly ordered elements, while running
  optimised bubble sort algorithm.

* Features of the artefact
+ Artefact provides demo for time complexity of bubble sort
+ User can see a randomly generated array in the beginning.
+ User can choose =Sorted=, =Reverse sorted= or =Random= array from the dropbox.
+ User can click the =Reset= button to generate a new array at any time.
+ User shall click the =Start= button to start the demo.
+ User can slide a =Slider= to adjust speed of the demo as per his/her convinience.
+ User can pause the demo by clicking on the =Pause= button.
+ User can pause and then click on =Next= manually to view the demo at his/her own speed.

* HTML Framework of Simple Bubble Sort
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Optimised Bubble Sort</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/bubble_css.css">
<link rel="stylesheet" href="http://exp-iiith.vlabs.ac.in/styles/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible"> Instructions </button>
  <div class = "content">
    <ul>
      <li>Click on the <b>Start</b> button to start the demo.</li>
      <li>Move the slider to adjust the speed of the demo.</li> 
      <li>Keep an eye on the number of comparisons.</li>
      <li>Use the dropbox to view demo for randomised, sorted, and reverse sorted arrays!</li>
    </ul> 
  </div>
</div>

#+END_SRC

*** Combobox for type of Array
This is a dropdown where the user can select which of type
array he wants to apply Bubble sort on.
#+NAME: array-type-holder
#+BEGIN_SRC html
<div class="buttons-wrapper" id="typeSelect">
  <p>Select type of array : &nbsp
    <select id = "order">
      <option value="random" selected="selected">Random</option>
      <option value="sorted">Sorted</option>
      <option value="reversesorted">Reverse Sorted</option>
    </select>
  </p>
</div>

#+END_SRC

*** Cards Holder
This is a cards holder, which contains 6 =<div class=card>=
elements as, we are keeping the size(6) of an array
static. In each of this div elements array elements are
generated as cards dynamically.
#+NAME: cards-holder
#+BEGIN_SRC html
<div id="cards"></div>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div  class="comment-box" id="comment-box-smaller">
  <b>Observations</b>
  <p id="ins"></p>
</div>

#+END_SRC

*** Slider
This is a slider to which =change_interval= method is
appended to control the demonstration speed of an artefact.
#+NAME: slider-container
#+BEGIN_SRC html
<div class="slidecontainer">
  <p>
    Min. Speed
    <input class="slider" type="range" min="100" max="2500" id="interval" value="1500">
    Max. Speed
  </p>
</div>

#+END_SRC

*** Buttons Wrapper
This is a div wrapped with buttons used for
performing/demonstrating an artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input class="button-input" type="button" value="Start" id="next" onClick="start_sort();" >
  <input class="button-input" type="button" value="Reset" id="reset">
  <input class="button-input" type="button" value="Pause" id="pause">
</div>

#+END_SRC

** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems
#+BEGIN_SRC html
  <script src="../js/bubble_time.js"></script>
  <script src="http://exp-iiith.vlabs.ac.in/styles/instruction-box.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle bubble_time.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>> 
    <div id="wrapper">
      <<array-type-holder>>
      <<cards-holder>>
      <div id="legend-comment-wrapper">
        <<comments-box>>
      </div>
      <<slider-container>>
      <<buttons-wrapper>>
    </div>
    <<js-elems>>
  </body>
</html>
#+END_SRC
