#+TITLE: Bubble Sort Exercise 
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =simple bubble sort exercise=
  interactive artefact(HTML).

* Features of the artefact
+ Artefact provides an exercse for simple bubble sort
+ User can see a randomly generated array of size 6 in the
  beginning. Size of the array is fixed.
+ User can click on =Start= button to start with the
  exercise.
+ User has the choice to click on =Next= or =Swap=.
+ You get a response towards each step you perform,
  i.e. correct or incorrect.
+ When the user thinks that he has completed sorting then he
  can click on =Submit=.
+ If the user wants to start again he can click on =Reset=.

* HTML Framework of Simple Bubble Sort
** Head Section Elements
Title, meta tags and all the links related to CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Bubble Sort</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/bubble_css.css">
<link rel="stylesheet" href="http://exp-iiith.vlabs.ac.in/styles/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible"> Instructions </button>
  <div class="content">
    <ul>
      <li>Click on the <b>Start</b> button to start the exercise.</li>
      <li>Click on the <b>Swap</b> or <b>Next</b> to perform these operations.</li> 
      <li>Click on <b>Submit</b> to check your result!</li>
      <li>Click on <b>Reset</b> to start over with a new set of numbers</li>
    </ul> 
  </div>
</div>

#+END_SRC

*** Question Holder
This will hold the question that is randomly generated
through Javascript.
#+NAME: question-holder
#+BEGIN_SRC html
<div id="question">
  <b>Question: </b>Sort the given array using Bubble sort.
</div>

#+END_SRC

*** Cards Holder
This is a cards holder, which contains 6 =<div class=card>=
elements as, we are keeping the size(6) of an array
static. In each of this div elements array elements are
generated as cards dynamically.
#+NAME: cards-holder
#+BEGIN_SRC html
<div id="cards"></div>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class = "comment-box" id="comment-box-smaller">
  <b>Observations</b>
  <p id="ins"> </p>
</div>

#+END_SRC

*** Buttons Wrapper
This is a div wrapped with buttons used for
performing/demonstrating an artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper"> 
  <input class="button-input" type="button" value="Start" id="start">
  <input class="button-input" type="button" value="Next" id="next">
  <input class="button-input" type="button" value="Swap" id="swap">
  <input class="button-input" type="button" value="Undo" id="undo">
  <input class="button-input" type="button" value="Reset" id="reset">
</div>

#+END_SRC

** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems
#+BEGIN_SRC html
  <script src="http://exp-iiith.vlabs.ac.in/styles/instruction-box.js"></script>
  <script src="../js/bubble_exercise.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle bubble_ex.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>> 
    <div id="wrapper">
      <<question-holder>>
      <<cards-holder>>
      <div id="legend-comment-wrapper">
        <<comments-box>>
      </div>
      <<buttons-wrapper>>
    </div>
    <<js-elems>>
  </body>
</html>
#+END_SRC
