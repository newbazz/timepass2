#+TITLE:  

   + org-templates
     + [[file:org-templates/level-0.org][level-0]]
     + [[file:org-templates/level-1.org][level-1]]
     + [[file:org-templates/level-2.org][level-2]]
     + [[file:org-templates/level-3.org][level-3]]
     + [[file:org-templates/level-4.org][level-4]]
     + [[file:org-templates/level-5.org][level-5]]
     + [[file:org-templates/tex-macros.org][tex-macros]]
   + runtime
     + css
       + [[file:runtime/css/bubble_css.org][Common Styles for Bubble Sort Interactive artefacts]]
     + html
       + [[file:runtime/html/bubble_exercise.org][Bubble Sort Exercise]]
       + [[file:runtime/html/bubble_practice.org][Bubble Sort Practice]]
       + [[file:runtime/html/optibubble_demo.org][Demonstration artefact for Optimised Bubble Sort]]
       + [[file:runtime/html/time_complexity_bubble.org][Demonstration artefact for Time complexity of Optimised Bubble Sort]]
       + [[file:runtime/html/bubble_demo.org][Demonstration of Simple Bubble Sort Artefact (HTML)]]
       + [[file:runtime/html/optibubble_exercise.org][Exercise artefact for Optimised Bubble Sort]]
       + [[file:runtime/html/optibubble_practice.org][Practice artefact for Optimised Bubble Sort]]
     + js
       + [[file:runtime/js/time_js.org][]]
       + [[file:runtime/js/optibubble_practice_js.org][]]
       + [[file:runtime/js/optibubble_exercise_js.org][]]
       + [[file:runtime/js/optibubble_demo_js.org][]]
       + [[file:runtime/js/bubble_practice_js.org][]]
       + [[file:runtime/js/bubble_exercise_js.org][]]
       + [[file:runtime/js/bubble_demo_js.org][Demonstration artefact for Bubble Sort (Javascript)]]
       + [[file:runtime/js/realization-catalog.org][Realization Catalog]]
   + [[file:index.org][Index to Artefacts and Realization Catalog]]
